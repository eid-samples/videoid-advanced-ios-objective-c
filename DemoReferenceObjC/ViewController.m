//
//  ViewController.m
//  DemoReferenceObjC
//
//  Created by Alberto Pérez on 21/09/2020.
//

#import "ViewController.h"

#import <VideoID/VideoID-Swift.h>
#import <VideoScan/VideoScan-Swift.h>
#import <FaceID/FaceID-Swift.h>

@interface ViewController ()<VideoDelegate>

@end

@implementation ViewController

typedef void(^CreateAuthorizationCompletionBlock)(BOOL success, NSString * _Nullable authorization, NSString * _Nullable errorCode);

static NSString * endpoint = @"https://etrust-sandbox.electronicid.eu/v2";
static NSString * bearer = @"your bearer";
static NSString * rAuthority = @"your rauthority";

- (void)startVideoIDWithAuthorization:(NSString *)authorization {
    
    Environment *env = [[Environment alloc] initWithUrl:endpoint
                                           autorization:authorization];
    
    VideoIDViewController *videoIDViewController = [[VideoIDViewController alloc] initWithEnvironment:env
                                                                                             language:nil
                                                                                              docType:62];
    [videoIDViewController setModalPresentationStyle:UIModalPresentationFullScreen];
    [videoIDViewController setDelegate:self];
    [self presentViewController:videoIDViewController animated:TRUE completion:nil];
    
}

- (void)startVideoScanWithAuthorization:(NSString *)authorization {
    
    Environment *env = [[Environment alloc] initWithUrl:endpoint
                                           autorization:authorization];
    
    
    VideoScanViewController *videoScanViewController = [[VideoScanViewController alloc] initWithEnvironment:env
                                                                                                   language:nil
                                                                                                    docType:62];
    [videoScanViewController setModalPresentationStyle:UIModalPresentationFullScreen];
    [videoScanViewController setDelegate:self];
    [self presentViewController:videoScanViewController animated:TRUE completion:nil];
    
}

- (void)startSmileIDWithAuthorization:(NSString *)authorization {
    
    Environment *env = [[Environment alloc] initWithUrl:endpoint
                                           autorization:authorization];
    
    
    FaceIDViewController *faceIDViewController = [[FaceIDViewController alloc] initWithEnvironment:env
                                                                                             language:nil];
    [faceIDViewController setModalPresentationStyle:UIModalPresentationFullScreen];
    [faceIDViewController setDelegate:self];
    [self presentViewController:faceIDViewController animated:TRUE completion:nil];
    
}

- (void)createAuthorization:(VideoService)service
            completionBlock:(nonnull CreateAuthorizationCompletionBlock)completionBlock {
    
    NSString *bearerValue = [NSString stringWithFormat:@"Bearer %@",bearer];
    
    NSString *videoServicePath = @"";
    
    if (service == VideoServiceVideoID) {
        
        videoServicePath = @"videoid.request";
    
    } else if (service == VideoServiceVideoScan) {
        
        videoServicePath = @"videoscan.request";
    
    } else if (service == VideoServiceSmileID) {
        
        videoServicePath = @"smileid.request";
    }
    
    
    NSString * serviceURL = [NSString stringWithFormat:@"%@/%@",endpoint,videoServicePath];
    
    NSDictionary<NSString *, NSString *> *auth = [NSDictionary dictionaryWithObjects:@[@"", @"Unattended",rAuthority]
                                                                                forKeys:@[@"tenantId", @"process", @"rauthorityId"]];
    
    NSURL *url = [NSURL URLWithString:serviceURL];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestReloadIgnoringLocalCacheData
                                                       timeoutInterval:30];
    
    [request addValue:bearerValue forHTTPHeaderField:@"Authorization"];
    [request addValue:@"application/json;charset=UTF-8" forHTTPHeaderField:@"content-type"];
    
    NSError * error = nil;
    
    NSData * jsonData = [NSJSONSerialization dataWithJSONObject:auth
                                                        options:NSJSONWritingPrettyPrinted
                                                          error:&error];
    
    [request setHTTPMethod:@"POST"];
    
    NSLog(@"Request %@", [request debugDescription]);
    NSString* newStr = [NSString stringWithUTF8String:[jsonData bytes]];
    NSLog(@"Body %@", newStr);
    
    NSURLSessionUploadTask *task = [[NSURLSession sharedSession] uploadTaskWithRequest:request
                                               fromData:jsonData
                                      completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        
        if (error != nil) {
            
        //ERROR
            completionBlock(NO,nil, error.debugDescription);
            NSLog(@"Request error: %@", error);
        }
        
        NSString* newStr = [NSString stringWithUTF8String:[data bytes]];
        NSLog(@"Response body %@", newStr);
        
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse*)response;
        
        if (httpResponse.statusCode == 200 && data != nil) {
            
            NSError * parsingError = nil;
            NSDictionary *jsonArray = [NSJSONSerialization JSONObjectWithData:data options:0
                                              error:&parsingError];
            
            if (!jsonArray) {
                
                completionBlock(NO, nil, error.debugDescription);
                NSLog(@"Error parsing JSON: %@", parsingError);
                
            } else {
               
                NSString *auth = [jsonArray objectForKey:@"authorization"];
                completionBlock(YES,auth, nil);
                
            }
            
        } else {
            
            NSMutableDictionary * jsonDict;
            
            if (data != nil) {
                
                NSError *error1;
                jsonDict = [NSJSONSerialization JSONObjectWithData:data
                                                           options:kNilOptions
                                                             error:&error1];
                
            }
            
            completionBlock(NO,[jsonDict objectForKey:@"error"], [jsonDict objectForKey:@"error_description"]);
            
        }
            
    }];
    
    [task resume];
    
    
}

#pragma mark -
#pragma mark User Interaction

- (IBAction)startVideoID {
    
    [self createAuthorization:VideoServiceVideoID
              completionBlock:^(BOOL success, NSString * _Nullable authorization, NSString * _Nullable errorCode) {
        
        if (success) {
            
            [self performSelectorOnMainThread:@selector(startVideoIDWithAuthorization:) withObject:authorization waitUntilDone:NO];
            
        } else {
            
            [self displayMessageWithTitle:@"Error" message:errorCode];
            
        }
        
    }];
    
}

- (IBAction)startVideoScan {
    
    [self createAuthorization:VideoServiceVideoScan
              completionBlock:^(BOOL success, NSString * _Nullable authorization, NSString * _Nullable errorCode) {
        
        if (success) {
            
            [self performSelectorOnMainThread:@selector(startVideoScanWithAuthorization:) withObject:authorization waitUntilDone:NO];
            
        } else {
            
            [self displayMessageWithTitle:@"Error" message:errorCode];
            
        }
        
    }];
    
}

- (IBAction)startSmileID {
    
    [self createAuthorization:VideoServiceSmileID
              completionBlock:^(BOOL success, NSString * _Nullable authorization, NSString * _Nullable errorCode) {
        
        if (success) {
            
            [self performSelectorOnMainThread:@selector(startSmileIDWithAuthorization:) withObject:authorization waitUntilDone:NO];
            
        } else {
            
            [self displayMessageWithTitle:@"Error" message:errorCode];
            
        }
        
    }];
    
}

- (void)displayMessageWithTitle:(NSString * _Nullable)title
                        message:(NSString * _Nullable)message {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        //update UI in main thread.
        
        UIAlertController* alert = [UIAlertController alertControllerWithTitle:title
                                                                       message:message
                                                                preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                              handler:^(UIAlertAction * action) {}];
        
        [alert addAction:defaultAction];
        [self presentViewController:alert animated:YES completion:nil];
    });
    
}

#pragma mark -
#pragma mark VideoDelegate

- (void)onCompleteWithVideoID:(NSString * _Nonnull)videoID {
    
    [self displayMessageWithTitle:@"Success" message:videoID];
    
}

- (void)onErrorWithCode:(NSString * _Nonnull)code message:(NSString * _Nullable)message {
    
    [self displayMessageWithTitle:code message:message];
    
}

@end
